# #100DaysOfCode Log - Round 1 - plumps

The log of my #100DaysOfCode challenge. Started on 1/6/2020.

## Log

### R1D1 
Continued freecodecamp Intermediate Algorithm Scripting. Tried to use maps and reduce to
improve my functional style.

- [dna.js](src/dna.js)
- [missing.js](src/missing.js)  << unfinished

### R1D2
