function pairElement(str) {
    const pairs = {
      G: "C",
      C: "G",
      T: "A",
      A: "T"
    };
  
    return str
            .split("")
            .map(c => [c, pairs[c]]);
  }
  
  console.log(pairElement("GCG"));
  