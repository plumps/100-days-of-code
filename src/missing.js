function fearNotLetter(str) {
  let acc = 0;

  for (let char of str.split("")) {
    let charCode = char.charCodeAt(0);

    if (charCode - acc == 2) {
      return String.fromCharCode(charCode - 1);
    } else {
      acc = charCode;
    }
  }
  return undefined;
}

// function fearNotLetter(str) {
//   var compare = str.charCodeAt(0),
//     missing;

//   str.split("").map(function(letter, index) {
//     if (str.charCodeAt(index) == compare) {
//       ++compare;
//     } else {
//       missing = String.fromCharCode(compare);
//     }
//   });

//   return missing;
}

// test here
fearNotLetter("abce");

console.log(fearNotLetter("abce"));
console.log(fearNotLetter("abcdefghjklmno"));
console.log(fearNotLetter("abcdefghijklmnopqrstuvwxyz"));
